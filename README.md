> THIS IS AN EXAMPLE, NOT A REAL LIBRARY

# Instructions

If you just clone **libempty** repository, remove the git folder and create a new one for your project :

    rm -rf .git
    git init
    git remote add origin git@<address>:<project>/<repository>

Replace all "MY NAME" by your real name in all copyright headers.

# Build

##Create build directory

    mkdir build
    cd build

##Build project

    cmake ..
    make -j 3

Debug build:

    cmake -DCMAKE_BUILD_TYPE=Debug ..
    make -j 3

##Install

    cmake -DCMAKE_INSTALL_PREFIX=<path> ..
    make
    make install

##Uninstall

    make uninstall

##Create documentation

    make doc
    firefox ./doc/html/index.html &

##Create tests

    cmake -DBUILD_TEST=ON ..
    make
    make test
